import {
  FeedLogCommandDto,
  LogRepositoryInterface,
} from '@/src/application/interfaces/out/logRepositoryInterface';
import { RedisClient } from 'bullmq';
import { FeedLogListResponse } from '@/src/application/interfaces/in/feedLogListUseCase';
import { TaskDispatcher } from '@/src/__lib/priorityQueue/taskDispatcher';
import { FEED_LOG_LIST_QUEUE_NAME } from '../../__configs/redisKey';
import { LoggerInterface } from '@/src/application/interfaces/loggerInterface';

export const makeDatabaseLogRepository = (
  redisClient: RedisClient,
  dispatcher: TaskDispatcher<FeedLogCommandDto, FeedLogListResponse>,
  logger: LoggerInterface
): LogRepositoryInterface => {
  const saveLogToDatabase = async (logData: FeedLogCommandDto) => {
    return redisClient.lpush(FEED_LOG_LIST_QUEUE_NAME, JSON.stringify(logData)).then(() => {
      logger.log(`[Database Log Repository]: saved log to database for id: "${logData.id}" /`);
    });
  };

  const storingTaskToRun = async (data: FeedLogCommandDto) => saveLogToDatabase(data);

  // register task
  dispatcher.registerTask(FEED_LOG_LIST_QUEUE_NAME, storingTaskToRun);

  return Object.freeze({
    save: async (logData: FeedLogCommandDto) => {
      return dispatcher.addJob(FEED_LOG_LIST_QUEUE_NAME, logData);
    },
  });
};
