import bodyParser from 'body-parser';

export const bodyParserMiddleware = bodyParser.json();

export const commonMiddlewares = [bodyParserMiddleware];
