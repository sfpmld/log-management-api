import request from 'supertest';
import makeInitializedDependencies from '../../../bootstrapDeps';
import makeInitializedUseCases from '../../../bootstrapUsecases';
import { startHttpServer, stopHttpServer } from '../../../../src/infrastructure/rest/httpServer';
import { config } from '../../../../src/__configs';
import { FakeTaskDispatcher } from '../../../__lib/priorityQueue/fakeTaskDispatcher';
import rawLogSample from '../../../../testSettings/fixtures/logSamples.json';

let app: unknown;
beforeAll(async () => {
  const deps = makeInitializedDependencies(config, FakeTaskDispatcher);
  const useCases = makeInitializedUseCases(config, deps, FakeTaskDispatcher);
  app = await startHttpServer(useCases, config);
});
afterAll(async () => {
  await stopHttpServer();
});
describe(`Story: Service can receive raw logs and being able to prettifying them before storing`, () => {
  describe(`Rule: POST "/" route can handle raw incoming logs`, () => {
    describe(`Given raw incoming logs`, () => {
      describe(`When processed by service`, () => {
        it(`Then it should succeed`, async () => {
          // Act
          const result = await request(app).post('/').send(rawLogSample);

          // Assert
          expect(result.status).toBe(200);
        });
      });
    });
  });
});
