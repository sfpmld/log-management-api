export type FeedLogListResponse = void;

export interface FeedLogListUseCaseInterface {
  handle: () => Promise<FeedLogListResponse>;
}
