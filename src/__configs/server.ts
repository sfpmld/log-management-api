export const serverConfig = {
  server: {
    hostname: process.env.HOSTNAME || 'localhost',
    port: Number.parseInt(process.env.PORT || '3000', 10),
  },
};
