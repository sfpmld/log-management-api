import rawLogSample from '../../../../testSettings/fixtures/logSamples.json';
import { LogParser } from '../logParser';

let sut: LogParser;
beforeEach(async () => {
  sut = new LogParser();
});
describe(`Given incoming raw log string`, () => {
  describe(`When process by Log Parser`, () => {
    it(`Then it should succeed`, async () => {
      // Act
      const result = sut.parse(rawLogSample);

      // Assert
      expect(result).toEqual({
        id: '0aaa9365-71ad-489b-b000-ecf98c87ba0b',
        service_name: 'api',
        process: 'api.2386.929618232808',
        load_avg_1m: '0.33',
        load_avg_5m: '0.27',
        load_avg_15m: '0.47',
      });
    });
  });
});
