import { ParsedConfig } from '@/src/__configs';
import { LoggerInterface } from '@/src/application/interfaces/loggerInterface';
import * as winston from 'winston';

export const makeLogger = (config: ParsedConfig): LoggerInterface => {
  const logger = winston.createLogger({
    level: config.LOGGING_LEVEL,
    format: winston.format.json(),
    defaultMeta: { service: config.SERVICE_NAME },
    transports: [
      new winston.transports.Console({
        format: winston.format.simple(),
      }),
    ],
  });

  return Object.freeze({
    log: (message: string): void => {
      logger.info(message);
    },
  });
};
