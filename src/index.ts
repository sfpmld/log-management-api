import { config } from './__configs';
import { startHttpServer } from './infrastructure/rest/httpServer';
import makeInitializedDependencies from './bootstrapDeps';
import makeInitializedUseCases from './bootstrapUsecases';

// Composition root
const dependencies = makeInitializedDependencies(config);
const useCases = makeInitializedUseCases(config, dependencies);

// start transport
startHttpServer(useCases, config);
