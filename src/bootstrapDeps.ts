import { Redis } from 'ioredis';
import { ParsedConfig } from './__configs';
import { LogRepositoryInterface } from './application/interfaces/out/logRepositoryInterface';
import { LogStorageInterface } from './application/interfaces/out/logStorageInterface';
import { makeDatabaseLogRepository } from './infrastructure/database/redis';
import { makeFileSystemLogRepository } from './infrastructure/storage/filesystem';
import { TaskDispatcher } from './__lib/priorityQueue/taskDispatcher';
import { makeLogger } from './infrastructure/logging';
import { LoggerInterface } from './application/interfaces/loggerInterface';

export type InfraDependenciesInterface = {
  logStorage: LogStorageInterface;
  logRepository: LogRepositoryInterface;
  logger: LoggerInterface;
};

const bootstrapInfraDependencies = (
  config: ParsedConfig,
  taskDispatcher: typeof TaskDispatcher = TaskDispatcher
): InfraDependenciesInterface => {
  const { redis } = config;
  const redisConnection = {
    host: redis.host,
    port: redis.port,
  };
  const redisClient = new Redis(redisConnection);
  const logger = makeLogger(config);

  return Object.freeze({
    logStorage: makeFileSystemLogRepository(new taskDispatcher(redisConnection)),
    logRepository: makeDatabaseLogRepository(
      redisClient,
      new taskDispatcher(redisConnection),
      logger
    ),
    logger,
  });
};

export default bootstrapInfraDependencies;
