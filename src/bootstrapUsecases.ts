import { makeStoreLogUseCase } from './application/usecases/storeLogUseCase';
import { makeFeedLogListUseCase } from './application/usecases/feedLogUseCase';
import { InfraDependenciesInterface } from './bootstrapDeps';
import { LogParser } from './application/services/logParser';
import { compute } from './__lib/slowComputation';
import { ParsedConfig } from './__configs';
import { LogEnricher } from './application/interfaces/out/logRepositoryInterface';
import { TaskDispatcher } from './__lib/priorityQueue/taskDispatcher';

const bootstrapUsecases = (
  config: ParsedConfig,
  deps: InfraDependenciesInterface,
  taskDispatcher: typeof TaskDispatcher = TaskDispatcher,
  logComputer: LogEnricher = compute
) => {
  const { redis } = config;
  const redisConfig = {
    host: redis.host,
    port: redis.port,
  };
  const logParser = new LogParser();

  return {
    storeLogUseCase: makeStoreLogUseCase(deps.logStorage, logParser, deps.logger),
    feedLogList: makeFeedLogListUseCase(
      deps.logStorage,
      deps.logRepository,
      new taskDispatcher(redisConfig),
      logComputer
    ),
  };
};

export default bootstrapUsecases;
