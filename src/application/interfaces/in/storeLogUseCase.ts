export type StoreLogCommand = { log: string[] };
export type StoreLogResponse = void;

export interface StoreLogUseCaseInterface {
  handle: (command: StoreLogCommand) => Promise<StoreLogResponse>;
}
