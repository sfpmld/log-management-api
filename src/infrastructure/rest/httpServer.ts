import express from 'express';
import { ParsedConfig } from '@/src/__configs';
import { middlewares } from './middlewares';
import makeLogRoutes, { AllUseCasesInterface } from './routes.ts';

const appFactory = (usecases: AllUseCasesInterface) => {
  const app = express();
  const router = express.Router();
  // mount middlewares
  app.use(middlewares.common);

  // init routes
  const routes = makeLogRoutes(usecases)(router);
  app.use(routes);

  return app;
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
let httpServerClosure: any;
export const startHttpServer = async (usecases: AllUseCasesInterface, config: ParsedConfig) => {
  const { port } = config.server;
  const app = appFactory(usecases);

  const server = new Promise((resolve, reject) => {
    try {
      resolve(
        app.listen(port, () => {
          resolve(this);
        })
      );
    } catch (e) {
      reject(e);
    }
  });

  return server
    .then(async (server) => {
      console.log(`Server listening on port ${port}`);
      httpServerClosure = server;
      return server;
    })
    .catch((err) => console.error(`Server start failed: ${err}`));
};

export const stopHttpServer = async () => {
  if (httpServerClosure) {
    await httpServerClosure.close();
  }
};
