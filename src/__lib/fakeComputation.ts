export async function fakeCompute<T extends object>(
  payload: T
): Promise<T & { slow_computation: string }> {
  return Object.assign(payload, { slow_computation: '0.0009878' });
}
