import _ from 'lodash';
import axios, { AxiosError } from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { sample } from './logGenerator';

const ENDPOINT = 'http://127.0.0.1:3000';

const httpRequest = async () => {
  try {
    const logSample = { log: sample(uuidv4()) };
    console.log('Sending request...', logSample);
    const response = await axios.post(ENDPOINT, logSample, { timeout: 1000 });
    console.log('Result => ', logSample, response.status);
  } catch (error) {
    if (error instanceof AxiosError) {
      if (error.code === 'ECONNABORTED') {
        console.log(`Remote server timed-out on ${ENDPOINT}`);
        return 'timeout';
      } else {
        console.log(`Connection refused on ${ENDPOINT}`, error);
        return 'error';
      }
    }
  }
};

_.times(500, httpRequest);
