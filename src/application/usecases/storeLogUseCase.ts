import { LogParserInterface } from '../interfaces/LogParserInterface';
import {
  StoreLogCommand,
  StoreLogResponse,
  StoreLogUseCaseInterface,
} from '../interfaces/in/storeLogUseCase';
import { LoggerInterface } from '../interfaces/loggerInterface';
import { LogStorageInterface } from '../interfaces/out/logStorageInterface';

export const makeStoreLogUseCase = (
  logStorage: LogStorageInterface,
  parser: LogParserInterface,
  logger: LoggerInterface
): StoreLogUseCaseInterface => {
  return Object.freeze({
    handle: async (command: StoreLogCommand): Promise<StoreLogResponse> => {
      const parsedLog = parser.parse(command);
      logger.log('[Store Log Usecase]: handling log storage for id: ', parsedLog.id);

      await logStorage.store(parsedLog);
    },
  });
};
