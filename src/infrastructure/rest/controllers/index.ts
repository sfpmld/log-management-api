import { Request, Response } from 'express';
import { AllUseCasesInterface } from '../routes.ts';

export const storeLogController =
  (usecases: AllUseCasesInterface) => async (req: Request, res: Response) => {
    const { body } = req;

    const response = await usecases.storeLogUseCase.handle(body);

    res.send(response);
  };

export const feedLogListController =
  (usecases: AllUseCasesInterface) => async (_: Request, res: Response) => {
    const response = await usecases.feedLogList.handle();
    res.send(response);
  };
