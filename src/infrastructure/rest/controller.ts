import { Request, Response } from 'express';
import { AllUseCasesInterface } from './routes.ts';

export const storeLogController =
  (usecases: AllUseCasesInterface) => async (req: Request, res: Response) => {
    const { body } = req;
    const { command } = body;

    const response = await usecases.storeLogUseCase.handle(command);

    res.send(response);
  };
