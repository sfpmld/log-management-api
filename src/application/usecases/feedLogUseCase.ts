import fs from 'fs';
import { FileName, LogStorageInterface } from '../interfaces/out/logStorageInterface';
import { LogEnricher, LogRepositoryInterface } from '../interfaces/out/logRepositoryInterface';
import { Log } from '@/src/model/Log';
import path from 'path';
import { FeedLogListUseCaseInterface } from '../interfaces/in/feedLogListUseCase';
import { TaskDispatcher } from '@/src/__lib/priorityQueue/taskDispatcher';

const parseStreamLog = (data: string | Buffer) => {
  if (typeof data === 'string') {
    return JSON.parse(data);
  } else {
    return JSON.parse(data.toString());
  }
};

export const makeFeedLogListUseCase = (
  logStorage: LogStorageInterface,
  logRepository: LogRepositoryInterface,
  dispatcher: TaskDispatcher<Log, void>,
  compute: LogEnricher
): FeedLogListUseCaseInterface => {
  const ENRICH_LOG_AND_FEED_LIST = 'enrich_log_and_feed_list';

  const feedLogTaskToRunInBackground = async (data: Log) => {
    const enrichedLog = await compute(data);
    await logRepository.save(enrichedLog);
  };
  // register task to run in background
  dispatcher.registerTask(ENRICH_LOG_AND_FEED_LIST, feedLogTaskToRunInBackground);

  const EnrichLogAndFeedLogList = async (fileName: FileName) => {
    const parsedDir = path.join(__dirname, '../../../parsed');
    const file = path.join(parsedDir, fileName);
    return new Promise((resolve, reject) => {
      return fs
        .createReadStream(file, { encoding: 'utf8' })
        .on('data', async (data) => {
          return dispatcher.addJob(ENRICH_LOG_AND_FEED_LIST, parseStreamLog(data));
        })
        .on('end', resolve)
        .on('error', reject);
    });
  };

  return Object.freeze({
    handle: async () => {
      const logFiles = await logStorage.getLogFiles();
      let pTask: Promise<any>[] = [];
      for (const fileName of logFiles) {
        pTask.push(EnrichLogAndFeedLogList(fileName));
      }
      return Promise.all(pTask).then(() => {});
    },
  });
};
