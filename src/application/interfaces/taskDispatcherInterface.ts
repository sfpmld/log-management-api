export interface TaskDispatcherInterface<T, U> {
  registerTask: (taskQueueName: string, fn: (data: any) => Promise<U>) => void;
  addJob: (taskQueueName: string, data: T) => Promise<any>;
}
