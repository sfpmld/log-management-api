import request from 'supertest';
import { Redis } from 'ioredis';
import makeInitializedDependencies from '../../../bootstrapDeps';
import makeInitializedUseCases from '../../../bootstrapUsecases';
import { startHttpServer, stopHttpServer } from '../../../../src/infrastructure/rest/httpServer';
import { config } from '../../../../src/__configs';
import { FEED_LOG_LIST_QUEUE_NAME } from '../../../../src/__configs/redisKey';

// fakes, mocks, stubs
import { FakeTaskDispatcher } from '../../../__lib/priorityQueue/fakeTaskDispatcher';
import { fakeCompute } from '../../../__lib/fakeComputation';

// helpers
const getLogListEntriesCount = (dbClient: Redis) => async (ListKey: string) => {
  return dbClient.llen(ListKey);
};
const flushDb = (dbClient: Redis) => async () => {
  return dbClient.flushall();
};

// tests
let app: unknown;
let backdoorDb: Redis;
beforeAll(async () => {
  backdoorDb = new Redis(config.redis);
  const deps = makeInitializedDependencies(config, FakeTaskDispatcher);
  const useCases = makeInitializedUseCases(config, deps, FakeTaskDispatcher, fakeCompute);
  app = await startHttpServer(useCases, config);
});
afterAll(async () => {
  await flushDb(backdoorDb)();
  await stopHttpServer();
});
describe(`Story: Service Enrich Parsed Logs before storing them in database`, () => {
  describe(`Rule: POST "/feedlist" route can process parsed logs to enrich them`, () => {
    describe(`Given parsed logs from raw logs handling`, () => {
      describe(`When processed by service`, () => {
        it(`Then it should have enrich logs entries`, async () => {
          // Act
          const result = await request(app).post('/feedlist').send({});

          // Assert
          expect(result.status).toBe(200);
          expect(
            await getLogListEntriesCount(backdoorDb)(FEED_LOG_LIST_QUEUE_NAME)
          ).toBeGreaterThan(0);
        });
      });
    });
  });
});
