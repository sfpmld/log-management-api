import { FeedLogListUseCaseInterface } from '@/src/application/interfaces/in/feedLogListUseCase';
import { StoreLogUseCaseInterface } from '@/src/application/interfaces/in/storeLogUseCase';
import { Router } from 'express';
import { feedLogListController, storeLogController } from '../controllers';

export type AllUseCasesInterface = {
  storeLogUseCase: StoreLogUseCaseInterface;
  feedLogList: FeedLogListUseCaseInterface;
};

const makeLogRoutes = (usecases: AllUseCasesInterface) => (router: Router) => {
  router
    .post('/feedlist', feedLogListController(usecases))

    .post('/', storeLogController(usecases));

  return router;
};

export default makeLogRoutes;
