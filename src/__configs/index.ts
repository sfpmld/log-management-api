import { serverConfig } from './server';
import { databaseConfig } from './database';
import { loggingConfig } from './logging';

export const config = Object.assign({}, serverConfig, databaseConfig, loggingConfig);

export type ParsedConfig = typeof config;
