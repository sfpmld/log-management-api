import { LogParserInterface } from '../interfaces/LogParserInterface';
import { StoreLogCommand } from '../interfaces/in/storeLogUseCase';
import { StoreLogCommandDto } from '../interfaces/out/logStorageInterface';

export class LogParser implements LogParserInterface {
  constructor() {}
  // method to parse the log
  parse(data: StoreLogCommand): StoreLogCommandDto {
    const log = data.log.reduce((acc, log) => {
      const keyWithoutHash = log.split('=')[0];
      const keyWithHashToRemove = log.split('=')[0].split('#')[1];
      const key = keyWithHashToRemove || keyWithoutHash;
      const value = log.split('=')[1];

      return {
        ...acc,
        [key]: value,
      };
    }, {} as StoreLogCommandDto);

    return log;
  }
}
