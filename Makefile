@PHONY: start-db stop-db test-unit test-spec test-all start

start-db:
	@echo "Starting the database"
	@docker compose -f ./testSettings/docker/docker-compose.test.yml up -d

stop-db:
	@echo "Stopping the database"
	@docker compose -f ./testSettings/docker/docker-compose.test.yml down -v --remove-orphans

prepare-test:
	@echo "Preparing fixtures for application tests"
	@npm run pretest

test-unit:
	@echo "Running unit tests (watch mode))"
	@make start-db
	@make prepare-test
	@npm run test:unit:watch
	@make stop-db

test-spec:
	@echo "Running system tests (watch mode)"
	@make start-db
	@make prepare-test
	@npm run test:spec:watch
	@make stop-db

test-all:
	@echo "Running all tests (watch mode)"
	@make start-db
	@make prepare-test
	@npm run test:watch
	@make stop-db

start-locally:
	@echo "Starting the application"
	@make start-db
	@npm run start
	@make stop-db

start-embedded:
	@echo "Running all tests (embedded mode with application running in docker container)"
	@DO='npm run start' docker compose -f ./testSettings/docker/docker-compose.test.yml -f ./testSettings/docker/docker-compose.full.yml up

emit-log:
	@echo "Starting the application in another terminal to emit log messages"
	@npm run logs:emit
