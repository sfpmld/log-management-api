import { TaskDispatcherInterface } from '@/src/application/interfaces/taskDispatcherInterface';
import { RedisConfig, TaskDispatcher } from './taskDispatcher';

export class FakeTaskDispatcher<T, U>
  extends TaskDispatcher<T, U>
  implements TaskDispatcherInterface<T, U>
{
  private registeredTasks: Map<string, (data: any) => Promise<U>> = new Map();

  constructor(config: RedisConfig) {
    super(config);
  }

  public registerTask(taskQueueName: string, fn: (data: any) => Promise<U>) {
    this.registeredTasks.set(taskQueueName, fn);
    console.log('registered task', taskQueueName, fn.toString());
  }

  public async addJob(taskQueueName: string, data: T): Promise<U> {
    const fn = this.registeredTasks.get(taskQueueName);
    if (!fn) {
      throw new Error(`task not registered: ${taskQueueName} is not registered`);
    }

    console.log('running task immediately for testing purposes with data: ', data);
    return fn(data);
  }
}
