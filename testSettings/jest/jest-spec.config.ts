module.exports = {
  displayName: 'Technical test for backend dev at ...',
  verbose: true,
  moduleFileExtensions: ['js', 'ts', 'tsx', 'json'],
  rootDir: '../../src',
  testRegex: '.spec.ts$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  testEnvironment: 'node',
  testPathIgnorePatterns: ['/node_modules/', '/dist/'],
};
