import { Log } from '@/src/model/Log';
import { StoreLogResponse } from '../in/storeLogUseCase';

export type StoreLogCommandDto = Log;
export type FileName = string;

export interface LogStorageInterface {
  store: (command: StoreLogCommandDto) => Promise<StoreLogResponse>;
  getLogFiles: () => Promise<FileName[]>;
}
