import { Log } from '@/src/model/Log';
import { FeedLogListResponse } from '../../usecases/feedLogUseCase';

export type EnrichedLogDto = Log & {
  slow_computation: string;
};
export type LogEnricher = (log: Log) => Promise<EnrichedLogDto>;

export type FeedLogCommandDto = EnrichedLogDto;

export interface LogRepositoryInterface {
  save: (log: EnrichedLogDto) => Promise<FeedLogListResponse>;
}
