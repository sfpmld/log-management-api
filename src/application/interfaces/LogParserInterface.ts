import { StoreLogCommand } from './in/storeLogUseCase';
import { StoreLogCommandDto } from './out/logStorageInterface';

export interface LogParserInterface {
  parse: (data: StoreLogCommand) => StoreLogCommandDto;
}
