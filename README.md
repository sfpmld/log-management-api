# Project Log Management API

## Introduction

This project aims to focus at most on Pure Dependency Inversion to ensure late binding and open the door to some flavor of SOLID principles. Or I try do the best to approach it with time imperative.

## Architecture

The project is design with a strong aspiration to follow some strong principles of "Tom Homberg" author of "Get Your Hand Dirty With Clean Architecture".

I didn't follow all the guideline for the sake of simplicity and time remaining to accomplish this assigment.But one main aspect that can particularly astonish is the principle of keeping separate class by usecases for example.

The reason, that talk a lot to me, is also from the book: "keeping them separate enhance Single Responsability Principle and maintainability".

**Notice**: that it is not how we do at my actual worksplace, and I can work in slightly different environments but when I do those kind of assigment, I often find it interested to continue to enhance reflexion and research around topic that really make us vibrate at the moment.

**Notice**: to deal with computation delay, I've choosen to use a priority queue strategy with bullmq utility.

## Getting Started

Follow the instruction present in the makefile according to your intention.

To start database dependencies :

    make start-db

To stop database dependencies :

    make stop-db

To run unit-test suite in (watch mode)

    make test-unit

To run component-test suite in (watch mode)

    make test-spec

To run all test suite in (watch mode)

    make test-all

To start the service locally

    make start-locally

To start the service inside docker cluster

    make start-embedded

To manually test the log emitting handling in real time (better run this one in another terminal)

    make emit-log

## Perspectives

I would have had a little more time to implement a robust error handling strategy with, eventually, a monadic fashion response handling. I would also add some more logging to enhance minimal monitoring when handling logs.
