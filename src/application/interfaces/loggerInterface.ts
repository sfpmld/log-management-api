export interface LoggerInterface {
  log(message: string, ...rest: any[]): void;
}
