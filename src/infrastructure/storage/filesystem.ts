import path from 'path';
import fs from 'fs/promises';
import {
  LogStorageInterface,
  StoreLogCommandDto,
} from '@/src/application/interfaces/out/logStorageInterface';
import { StoreLogResponse } from '@/src/application/interfaces/in/storeLogUseCase';
import { TaskDispatcher } from '@/src/__lib/priorityQueue/taskDispatcher';

export const makeFileSystemLogRepository = (
  dispatcher: TaskDispatcher<StoreLogCommandDto, StoreLogResponse>
): LogStorageInterface => {
  const STORE_LOG_QUEUE_NAME = 'storeLog';

  const generateLogPath = (id: string) => path.join(__dirname, `../../../parsed/${id}.json`);

  const storeLogToFileSystem = async (logData: StoreLogCommandDto): Promise<StoreLogResponse> => {
    const logPath = generateLogPath(logData.id);
    const logDir = path.dirname(logPath);

    await fs.mkdir(logDir, { recursive: true });
    await fs.writeFile(logPath, JSON.stringify(logData), 'utf8');
  };

  const storingTaskToRun = async (data: StoreLogCommandDto) => await storeLogToFileSystem(data);

  // register task
  dispatcher.registerTask(STORE_LOG_QUEUE_NAME, storingTaskToRun);

  return Object.freeze({
    store: async (logData: StoreLogCommandDto): Promise<StoreLogResponse> => {
      return dispatcher.addJob(STORE_LOG_QUEUE_NAME, logData);
    },
    getLogFiles: async (): Promise<string[]> => {
      const parsedDir = path.join(__dirname, '../../../parsed');
      const files = await fs.readdir(parsedDir);
      return files;
    },
  });
};
