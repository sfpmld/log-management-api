import { TaskDispatcherInterface } from '@/src/application/interfaces/taskDispatcherInterface';
import { Job, Queue, Worker } from 'bullmq';

export type RedisConfig = {
  host: string;
  port: number;
};
type defaultBullConfig = {
  removeOnComplete: boolean;
  removeOnFail: boolean;
};

export class TaskDispatcher<T, U = void> implements TaskDispatcherInterface<T, U> {
  protected queue: Queue<T>;
  protected worker: Worker<T>;
  protected connection: {
    connection: RedisConfig;
  };
  protected defaultConfig: defaultBullConfig;

  constructor(config: RedisConfig) {
    this.connection = {
      connection: {
        host: config.host,
        port: config.port,
      },
    };
  }

  public registerTask(taskQueueName: string, fn: (data: any) => Promise<U>) {
    this.createQueue(taskQueueName);
    this.worker = new Worker<T, U>(
      taskQueueName,
      async (job: Job) => {
        return await fn(job.data);
      },
      {
        ...this.connection,
        concurrency: 1,
      }
    );
  }

  public async addJob(taskQueueName: string, data: T): Promise<any> {
    return this.queue.add(taskQueueName, data, this.defaultConfig);
  }

  private createQueue(taskQueueName: string) {
    this.queue = new Queue(taskQueueName, this.connection);
  }
}
