import { commonMiddlewares } from './commonMiddlewares';

export const middlewares = {
  common: commonMiddlewares,
};
